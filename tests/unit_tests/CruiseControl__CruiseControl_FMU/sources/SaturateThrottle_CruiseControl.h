/* $*************** KCG Version 6.1.3 (build i6) ****************
** Command: s2c613 -config C:/Program Files (x86)/Esterel Technologies/Esterel SCADE 6.4.2i25/examples/CruiseControl/CruiseControl/KCG\kcg_s2c_config.txt
** Generation date: 2013-06-12T22:26:25
*************************************************************$ */
#ifndef _SaturateThrottle_CruiseControl_H_
#define _SaturateThrottle_CruiseControl_H_

#include "kcg_types.h"

/* =====================  no input structure  ====================== */


/* CruiseControl::SaturateThrottle */
extern void SaturateThrottle_CruiseControl(
  /* CruiseControl::SaturateThrottle::ThrottleIn */tPercent_CarType ThrottleIn,
  /* CruiseControl::SaturateThrottle::ThrottleOut */tPercent_CarType *ThrottleOut,
  /* CruiseControl::SaturateThrottle::Saturate */kcg_bool *Saturate);

#endif /* _SaturateThrottle_CruiseControl_H_ */
/* $*************** KCG Version 6.1.3 (build i6) ****************
** SaturateThrottle_CruiseControl.h
** Generation date: 2013-06-12T22:26:25
*************************************************************$ */

