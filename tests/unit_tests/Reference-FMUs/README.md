# Reference-FMUs - Functional Mock-up Units for development, testing and debugging

These FMUs are a set of hand-coded FMUs for development, testing and debugging from [modelica/Reference-FMUs](https://github.com/modelica/Reference-FMUs).

Each FMU has been generated for each variant:

* a specific FMU version
* type for FMI v1

These FMUs were generated in October 2024 from Reference-FMUs v0.0.34 .
