                            List of FMUs
To allow testing of import functionality of FMI for Model Exchange version 1.0, 
a number of test FMUs is provided from different tool vendors that export FMUs.

All FMUs were taken from the official site of FMI.
https://www.fmi-standard.org/

//============================================================================//
FMUs exported by SCADE:

Available Platforms:
Linux 64/32 bits
Windows 64/32 bits 
- SCADE_CruiseControl__CruiseControl_FMU.fmu

//============================================================================//
FMUs exported by CATIA:

Available Platforms:
Windows 64/32 bits: 
- CATIA_Modelica_Electrical_Digital_Examples_DFFREG.fmu
- CATIA_Modelica_Mechanics_MultiBody_Examples_Loops_Engine1b.fmu
- CATIA_Modelica_Mechanics_Rotational_Examples_CoupledClutches.fmu

//============================================================================//
FMUs exported by Dymola:

Available Platforms:
Windows 64/32 bits:
- Dymola_Modelica_Electrical_Digital_Examples_DFFREG.fmu
- Dymola_Modelica_Mechanics_MultiBody_Examples_Loops_Engine1b.fmu
- Dymola_Modelica_Mechanics_Rotational_Examples_CoupledClutches.fmu

//============================================================================//
FMUs exported by JModelica:

Available Platforms:
Linux 64 bits: 
- JModelica_CoupledClutches.fmu
- JModelica_Modelica_Blocks_Examples_PID_Controller.fmu

//============================================================================//
FMUs exported by MapleSIM:

Available Platforms:
Linux 32 bits:
- MapleSIM_CoupledClutches_Linux32bits.fmu
- MapleSIM_Rectifier1_0_Linux32bits.fmu
Linux 64 bits:
- MapleSIM_CoupledClutches_Linux64bits.fmu 
- MapleSIM_Rectifier1_0_Linux64bits.fmu
Windows 64/32 bits:
- MapleSIM_CoupledClutches_Win32_64bits.fmu
- MapleSIM_Rectifier1_0_Win32_64bits.fmu 

//============================================================================//
FMUs exported by SimulationX:

Available Platforms:
Windows 64/32 bits
- SimulationX_ControlledTemperature.fmu
- SimulationX_CoupledClutches.fmu 
- SimulationX_Engine1b.fmu 
- SimulationX_Rectifier.fmu
