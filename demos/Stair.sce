//
// This file is released under the 3-clause BSD license. See COPYING-BSD.
//

schemapath = fullfile(fmigetPath(), "tests", "unit_tests", "Reference-FMUs", "Stair.xcos");
xcos(schemapath);
