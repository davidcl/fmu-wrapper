Toolbox: fmu_wrapper

Title: Xcos FMU wrapper

Summary: Add some functionalities to use FMU blocks inside Scilab/Xcos. 
         Export of FMU from Xcos. 

Version: 2.0.3

Author:	Clément DAVID <clement.david@3ds.com>
	Vladislav TRUBKIN <uladzislau.trubkin@scilab-enterprises.com>
	Frederic MAHIANT <fred@raspinloop.org>

Maintainer: Clément DAVID <clement.david@3ds.com>

Category: Xcos

Entity: Dassault Systèmes

WebSite: http://www.scilab.org

License: CeCILL

ScilabVersion: >= 2024.0.0

Depends: 

Date: 2024-10-11

Description: 
 import FMUs: Allow the user to add FMU blocks inside Xcos. This toolbox provides an easy way to use Functional Mock-up Interface for Model Exchange and Co-Simulation. 
             In Xcos it should to use the palette "Functional Mock-up Interfaces", the user is able to simulate the Functional Mock-up Units(dynamic system models). 
             It is possible to use several instances of a model and to connect any models hierarchically together.
             In Scilab it should to use the function "fmiSimulate".
 export FMUs: Xcos model can be exported as an FMU for Model Exchange or for Co-Simulation. 
            Actually, only generation of FMUs version 1.0 of FMI is supported in Xcos. 
            Code generation works by translation the Xcos super-block in FMU. Generated model can be imported in Xcos or in an another simulation tool.

 Main features: 
 - Support of FMI 1.0 for Model Exchange and Co-Simulation.
 - Support of FMI 2.0 for Model Exchange and Co-Simulation.
 - Simulation for compiled FMUs: an FMU must have at minimum one binary corresponding to working platform.     
 - Simulation in Xcos with using any type of explicit solvers.
 - Simulation in Scilab with fixed step integration (Method of Euler).
 
 Start toolbox:
 exec builder.sce
 exec loader.sce
 
 Stop toolbox:
 exec unloader.sce
 
 For more information:
 help FMU 

