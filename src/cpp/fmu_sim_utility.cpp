#include "fmu_wrapper.hxx"

extern "C" {
#include "dynlib_fmucpp_wrapper.h"

#include "fmiPlatformTypes.h"
#include "fmiFunctionTypes.h"
#include "fmiFunctions.h"

#include "fmi2TypesPlatform.h"
#include "fmi2FunctionTypes.h"
#include "fmi2Functions.h"

#include "scicos.h"
#include "scicos_block4.h"
#include "Scierror.h"
#include "scicos_malloc.h"
#include "scicos_free.h"
#include "lasterror.h"
#include "sciprint.h"
#include "fmu_sim_utility.h"
}

#include "UTF8.hxx"

#include <cstring>

// mask for negatedAlias or discrete variables
#define NEGATED_ALIAS   (1 << 0)
#define DISCRETE        (1 << 1)

// set start values for variables with initial="exact" (as default or preset)
int fmiSet_parameters(scicos_block *block, scicos_flag flag)
{
    uint32_t *sciRealRefs = NULL, *sciIntRefs = NULL, *sciBoolRefs = NULL;
    fmiReal *rpar = NULL;
    fmiInteger *ipar = NULL;
    int *sciBoolValues = NULL;
    size_t realRefsSize, intRefsSize, boolRefsSize;
    int status;

    size_t i, j;
    workFMU *structFmu = *(workFMU **)(block->work);
    FmuV2Common* fmu;
    if (structFmu->version != workFMU::fmiV2)
    {
        return INT32_MAX - 1;
    }
    if (structFmu->type != workFMU::CoSimulation)
    {
        fmu = structFmu->fmuCS2;
    }
    else if (structFmu->type != workFMU::ModelExchange)
    {
        fmu = structFmu->fmuME2;
    }
    else
    {
        return INT32_MAX - 2;
    }

    // real values
    sciRealRefs = Getuint32OparPtrs(block, 6); // real refs
    realRefsSize = GetOparSize(block, 6, 1) * GetOparSize(block, 6, 2); // real refs size
    rpar = GetRparPtrs(block); // real values
    // integer values
    sciIntRefs = Getuint32OparPtrs(block, 7);
    intRefsSize = GetOparSize(block, 7, 1) * GetOparSize(block, 7, 2);
    ipar = GetIparPtrs(block);
    // boolean values
    sciBoolRefs = Getuint32OparPtrs(block, 8);
    boolRefsSize = GetOparSize(block, 8, 1) * GetOparSize(block, 8, 2);
    sciBoolValues = Getint32OparPtrs(block, 9); // boolean values
    
    // real
    if (realRefsSize > 0)
    {
        fmi2Real* realValues = (fmi2Real*) scicos_malloc(realRefsSize/2 * sizeof(fmi2Real));

        // check negated alias and store referenceValues
        for (i = 0, j = realRefsSize/2; j < realRefsSize; i++, j++)
        {
            if (sciRealRefs[j] > 0)
            {
                // negated alias set to negative parameter value
                realValues[i] = -rpar[i];
            }
            else
            {
                realValues[i] = rpar[i];
            }

            FMU_SIM_PRINT("fmiSet_parameters real valueReference %d set to %lf\n", sciRealRefs[i], realValues[i]);
        }
        // set real values
        status = fmu->fmi2SetReal(structFmu->instance, sciRealRefs, i, realValues);
        if (status > fmi2Discard)
        {
            free_instance(block, -3, "fmi2SetReal");
            return status;
        }

        scicos_free(realValues);
    }
    // integers
    if (intRefsSize > 0)
    {
        fmi2Integer* intValues = (fmi2Integer*) scicos_malloc(intRefsSize/2 * sizeof(fmi2Integer));

        // check negated alias and store referenceValues
        for (i = 0, j = intRefsSize/2; j < intRefsSize; i++, j++)
        {
            if (sciIntRefs[j] > 0)
            {
                // negated alias set to negative parameter value
                intValues[i] = -ipar[i];
            }
            else
            {
                intValues[i] = ipar[i];
            }

            FMU_SIM_PRINT("fmiSet_parameters integer valueReference %d set to %lf\n", sciIntRefs[i], intValues[i]);
        }
        // set integer values
        status = fmu->fmi2SetInteger(structFmu->instance, sciIntRefs, i, ipar);
        if (status > fmi2Warning)
        {
            free_instance(block, -3, "fmi2SetInteger");
            return status;
        }

        scicos_free(intValues);
    }
    // boolean
    if (boolRefsSize > 0)
    {
        fmi2Boolean* boolValues = (fmi2Boolean*) scicos_malloc(boolRefsSize/2 * sizeof(fmi2Boolean));

        // check negated alias and store referenceValues
        for (i = 0, j = boolRefsSize/2; j < boolRefsSize; i++, j++)
        {
            if (sciBoolRefs[j] > 0)
            {
                // negated alias set to negative parameter value
                if (sciBoolValues[i] == 0)
                {
                    boolValues[i] = fmi2True;
                }
                else
                {
                    boolValues[i] = fmi2False;
                }
            }
            else
            {
                if (sciBoolValues[i] != 0)
                {
                    boolValues[i] = fmi2True;
                }
                else
                {
                    boolValues[i] = fmi2False;
                }
            }
            
            FMU_SIM_PRINT("fmiSet_parameters boolean valueReference %d set to %lf\n", sciBoolRefs[i], boolValues[i]);
        }
        // set boolean values
        status = fmu->fmi2SetBoolean(structFmu->instance, sciBoolRefs, i, boolValues);
        if (status > fmi2Warning)
        {
            free_instance(block, -3, "fmi2SetBoolean");
            return status;
        }

        scicos_free(boolValues);
    }
    return 0;
}


// set input Values
int fmiSet_values(scicos_block *block, scicos_flag flag, int continuousMode, int *declaredTypes, int* blockTypes, size_t inpSize, uint32_t *sciInputRefs, void** from)
{
    fmiStatus status = fmiOK;
    fmi2Status status2 = fmi2OK;
    fmiStatus globalStatus = fmiOK;
    fmi2Status globalStatus2 = fmi2OK;

    size_t i, j, k;
    workFMU *structFmu = *(workFMU **)(block->work);
    if (structFmu == nullptr)
    {
        return fmiFatal;
    }

    for (i = 0, k = inpSize / 2, j = 0; k < inpSize; i++, j++, k++)
    {
        if (declaredTypes[i] == 1) // real
        {
            double* asDouble = ((double *) from[i]);
            double valueReal;

            if ((sciInputRefs[k] & NEGATED_ALIAS) == NEGATED_ALIAS) // negatedAlias
            {
                valueReal = -*asDouble;
            }
            else
            {
                valueReal = *asDouble;
            }

            if ((sciInputRefs[k] & DISCRETE) == DISCRETE) // discrete variable
            {
                if (structFmu->type == workFMU::ModelExchange && continuousMode)
                {
                    // do not set in continuous mode
                    continue;
                }
            }

            FMU_SIM_PRINT("fmiSet_values real valueReference %d set to %lf\n", sciInputRefs[j], valueReal);

            if (structFmu->version == workFMU::fmiV1 && structFmu->type == workFMU::ModelExchange)
            {
                status = structFmu->fmuME1->fmiSetReal((structFmu->instance), &sciInputRefs[j], 1, &valueReal);
            }
            else if (structFmu->version == workFMU::fmiV1 && structFmu->type == workFMU::CoSimulation)
            {
                status = structFmu->fmuCS1->fmiSetReal((structFmu->instance), &sciInputRefs[j], 1, &valueReal);
            }
            else if (structFmu->version == workFMU::fmiV2 && structFmu->type == workFMU::ModelExchange)
            {
                status2 = structFmu->fmuME2->fmi2SetReal((structFmu->instance), &sciInputRefs[j], 1, &valueReal);
            }
            else if (structFmu->version == workFMU::fmiV2 && structFmu->type == workFMU::CoSimulation)
            {
                status2 = structFmu->fmuCS2->fmi2SetReal((structFmu->instance), &sciInputRefs[j], 1, &valueReal);
            }

            // store the highest possible error
            if (status > globalStatus)
            {
                globalStatus = status;
            }
            if (status2 > globalStatus2)
            {
                globalStatus2 = status2;
            }
        }
        else if ((declaredTypes[i] == 2) | (declaredTypes[i] == 5)) // integer or enumeration
        {
            int* asInt = (int *) from[i];
            int valueInt;

            if ((sciInputRefs[k] & NEGATED_ALIAS) == NEGATED_ALIAS) // negatedAlias
            {
                // mapped as double, convert
                if (blockTypes[i] == SCSREAL_N)
                {
                    double* d = (double*) from[i];
                    valueInt = (int) -*d;
                } else
                {
                    valueInt = -*asInt;
                }
            }
            else
            {
                // mapped as double, convert
                if (blockTypes[i] == SCSREAL_N)
                {
                    double* d = (double*) from[i];
                    valueInt = (int) *d;
                } else
                {
                    valueInt = *asInt;
                }
            }

            if ((sciInputRefs[k] & DISCRETE) == DISCRETE) // discrete variable
            {
                if (structFmu->type == workFMU::ModelExchange && continuousMode)
                {
                    // do not set in continuous mode
                    continue;
                }
            }
            
            FMU_SIM_PRINT("fmiSet_values integer valueReference %d set to %d\n", sciInputRefs[j], valueInt);
            
            if (structFmu->version == workFMU::fmiV1 && structFmu->type == workFMU::ModelExchange)
            {
                status = structFmu->fmuME1->fmiSetInteger((structFmu->instance), &sciInputRefs[j], 1, &valueInt);
            }
            else if (structFmu->version == workFMU::fmiV1 && structFmu->type == workFMU::CoSimulation)
            {
                status = structFmu->fmuCS1->fmiSetInteger((structFmu->instance), &sciInputRefs[j], 1, &valueInt);
            }
            else if (structFmu->version == workFMU::fmiV2 && structFmu->type == workFMU::ModelExchange)
            {
                // 0 is not a valid enumeration value, do not set at re-initialization where everything is set to 0 first
                if ((flag == Initialization || flag == ReInitialization) && declaredTypes[i] == 5 && valueInt == 0)
                {
                    FMU_SIM_PRINT("fmiSet_values enumeration set to 0, ignored\n");
                }
                else
                {
                    status2 = structFmu->fmuME2->fmi2SetInteger((structFmu->instance), &sciInputRefs[j], 1, &valueInt);
                }
            }
            else if (structFmu->version == workFMU::fmiV2 && structFmu->type == workFMU::CoSimulation)
            {
                // 0 is not a valid enumeration value, do not set at re-initialization where everything is set to 0 first
                if ((flag == Initialization || flag == ReInitialization) && declaredTypes[i] == 5 && valueInt == 0)
                {
                    FMU_SIM_PRINT("fmiSet_values enumeration set to 0, ignored\n");
                }
                else
                {
                    status2 = structFmu->fmuCS2->fmi2SetInteger((structFmu->instance), &sciInputRefs[j], 1, &valueInt);
                }
            }

            // store the highest possible error
            if (status > globalStatus)
            {
                globalStatus = status;
            }
            if (status2 > globalStatus2)
            {
                globalStatus2 = status2;
            }
        }
        else if (declaredTypes[i] == 3) // boolean, compatible with fmi2 as fmi2True == fmiTrue and fmi2False == fmiFalse
        {
            char* asChar = (char *) from[i];
            fmiBoolean v1Bool;
            fmi2Boolean v2Bool;

            if ((sciInputRefs[k] & NEGATED_ALIAS) == NEGATED_ALIAS) // negatedAlias
            {
                // mapped as double, convert
                if (blockTypes[i] == SCSREAL_N)
                {
                    double* d = (double*) asChar;
                    if (*d) {
                        v1Bool = fmiFalse;
                        v2Bool = fmi2False;
                    } else {
                        v1Bool = fmiTrue;
                        v2Bool = fmi2True;
                    }
                }
                else
                {
                    if (*asChar) {
                        v1Bool = fmiFalse;
                        v2Bool = fmi2False;
                    } else {
                        v1Bool = fmiTrue;
                        v2Bool = fmi2True;
                    }
                }
            }
            else
            {
                // mapped as double, convert
                if (blockTypes[i] == SCSREAL_N)
                {
                    double* d = (double*) asChar;
                    if (*d) {
                        v1Bool = fmiTrue;
                        v2Bool = fmi2True;
                    } else {
                        v1Bool = fmiFalse;
                        v2Bool = fmi2False;
                    }
                }
                else
                {
                    if (*asChar) {
                        v1Bool = fmiTrue;
                        v2Bool = fmi2True;
                    } else {
                        v1Bool = fmiFalse;
                        v2Bool = fmi2False;
                    }
                }
            }

            if ((sciInputRefs[k] & DISCRETE) == DISCRETE) // discrete variable
            {
                if (structFmu->type == workFMU::ModelExchange && continuousMode)
                {
                    // do not set in continuous mode
                    continue;
                }
            }
            
            FMU_SIM_PRINT("fmiSet_values boolean valueReference %d set to %d\n", sciInputRefs[j], v2Bool);
            
            if (structFmu->version == workFMU::fmiV1 && structFmu->type == workFMU::ModelExchange)
            {
                status = structFmu->fmuME1->fmiSetBoolean((structFmu->instance), &sciInputRefs[j], 1, &v1Bool);
            }
            else if (structFmu->version == workFMU::fmiV1 && structFmu->type == workFMU::CoSimulation)
            {
                status = structFmu->fmuCS1->fmiSetBoolean((structFmu->instance), &sciInputRefs[j], 1, &v1Bool);
            }
            else if (structFmu->version == workFMU::fmiV2 && structFmu->type == workFMU::ModelExchange)
            {
                status2 = structFmu->fmuME2->fmi2SetBoolean((structFmu->instance), &sciInputRefs[j], 1, &v2Bool);
            }
            else if (structFmu->version == workFMU::fmiV2 && structFmu->type == workFMU::CoSimulation)
            {
                status2 = structFmu->fmuCS2->fmi2SetBoolean((structFmu->instance), &sciInputRefs[j], 1, &v2Bool);
            }

            // store the highest possible error
            if (status > globalStatus)
            {
                globalStatus = status;
            }
            if (status2 > globalStatus2)
            {
                globalStatus2 = status2;
            }
        }
        else if (declaredTypes[i] == 4) // string
        {
            int maxStringLen = GetInPortSize(block, i, 2);
            char* asCharPtr = (char *) from[i];
            asCharPtr[maxStringLen] = '\0'; // ensure string termination

            if ((sciInputRefs[k] & DISCRETE) == DISCRETE) // discrete variable
            {
                if (structFmu->type == workFMU::ModelExchange && continuousMode)
                {
                    // do not set in continuous mode
                    continue;
                }
            }
            
            FMU_SIM_PRINT("fmiSet_values string valueReference %d set to %s\n", sciInputRefs[j], asCharPtr);

            if (structFmu->version == workFMU::fmiV1 && structFmu->type == workFMU::ModelExchange)
            {
                status = structFmu->fmuME1->fmiSetString((structFmu->instance), &sciInputRefs[j], 1, &asCharPtr);
            }
            else if (structFmu->version == workFMU::fmiV1 && structFmu->type == workFMU::CoSimulation)
            {
                status = structFmu->fmuCS1->fmiSetString((structFmu->instance), &sciInputRefs[j], 1, &asCharPtr);
            }
            else if (structFmu->version == workFMU::fmiV2 && structFmu->type == workFMU::ModelExchange)
            {
                status2 = structFmu->fmuME2->fmi2SetString((structFmu->instance), &sciInputRefs[j], 1, &asCharPtr);
            }
            else if (structFmu->version == workFMU::fmiV2 && structFmu->type == workFMU::CoSimulation)
            {
                status2 = structFmu->fmuCS2->fmi2SetString((structFmu->instance), &sciInputRefs[j], 1, &asCharPtr);
            }

            // store the highest possible error
            if (status > globalStatus)
            {
                globalStatus = status;
            }
            if (status2 > globalStatus2)
            {
                globalStatus2 = status2;
            }
        }
    }

    // report the highest possible error
    if (globalStatus != fmiOK)
    {
        return globalStatus;
    }
    return globalStatus2;
}

// get output Values
int fmiGet_values(scicos_block *block, int* declaredTypes, int* blockTypes, size_t outSize, uint32_t *sciOutputRefs, void** into)
{
    fmiStatus status = fmiOK;
    fmi2Status status2 = fmi2OK;
    fmiStatus globalStatus = fmiOK;
    fmi2Status globalStatus2 = fmi2OK;

    size_t i, j, k;
    workFMU *structFmu = *(workFMU **)(block->work);
    if (structFmu == nullptr)
    {
        return fmi2Fatal;
    }

    for (i = 0, k = outSize / 2, j = 0; k < outSize; i++, j++, k++)
    {
        if (declaredTypes[i] == 1) // Real
        {
            double *valueRealSci = (double *) into[i];
            double valueReal;
            if (structFmu->version == workFMU::fmiV1 && structFmu->type == workFMU::ModelExchange)
            {
                status = structFmu->fmuME1->fmiGetReal((structFmu->instance), &sciOutputRefs[j], 1, &valueReal);
            }
            else if (structFmu->version == workFMU::fmiV1 && structFmu->type == workFMU::CoSimulation)
            {
                status = structFmu->fmuCS1->fmiGetReal((structFmu->instance), &sciOutputRefs[j], 1, &valueReal);
            }
            else if (structFmu->version == workFMU::fmiV2 && structFmu->type == workFMU::ModelExchange)
            {
                status2 = structFmu->fmuME2->fmi2GetReal((structFmu->instance), &sciOutputRefs[j], 1, &valueReal);
            }
            else if (structFmu->version == workFMU::fmiV2 && structFmu->type == workFMU::CoSimulation)
            {
                status2 = structFmu->fmuCS2->fmi2GetReal((structFmu->instance), &sciOutputRefs[j], 1, &valueReal);
            }

            FMU_SIM_PRINT("fmiGet_values real valueReference %d set to %lf\n", sciOutputRefs[j], valueReal);

            // store the highest possible error
            if (status > globalStatus)
            {
                globalStatus = status;
            }
            if (status2 > globalStatus2)
            {
                globalStatus2 = status2;
            }

            if ((sciOutputRefs[k] & NEGATED_ALIAS) == NEGATED_ALIAS) // negatedAlias
            {
                *valueRealSci = -valueReal;
            }
            else
            {
                *valueRealSci = valueReal;
            }
        }
        else if ((declaredTypes[i] == 2) | (declaredTypes[i] == 5)) // integer or enumeration
        {
            int valueInt;
            if (structFmu->version == workFMU::fmiV1 && structFmu->type == workFMU::ModelExchange)
            {
                status = structFmu->fmuME1->fmiGetInteger((structFmu->instance), &sciOutputRefs[j], 1, &valueInt);
            }
            else if (structFmu->version == workFMU::fmiV1 && structFmu->type == workFMU::CoSimulation)
            {
                status = structFmu->fmuCS1->fmiGetInteger((structFmu->instance), &sciOutputRefs[j], 1, &valueInt);
            }
            else if (structFmu->version == workFMU::fmiV2 && structFmu->type == workFMU::ModelExchange)
            {
                status2 = structFmu->fmuME2->fmi2GetInteger((structFmu->instance), &sciOutputRefs[j], 1, &valueInt);
            }
            else if (structFmu->version == workFMU::fmiV2 && structFmu->type == workFMU::CoSimulation)
            {
                status2 = structFmu->fmuCS2->fmi2GetInteger((structFmu->instance), &sciOutputRefs[j], 1, &valueInt);
            }

            FMU_SIM_PRINT("fmiGet_values integer valueReference %d set to %d\n", sciOutputRefs[j], valueInt);

            // store the highest possible error
            if (status > globalStatus)
            {
                globalStatus = status;
            }
            if (status2 > globalStatus2)
            {
                globalStatus2 = status2;
            }

            if ((sciOutputRefs[k] & NEGATED_ALIAS) == NEGATED_ALIAS) // negatedAlias
            {
                if (blockTypes[i] == SCSREAL_N)
                {
                    // mapped as double
                    double* negated = (double*) into[i];
                    *negated = -valueInt;
                }
                else
                {
                    int* negated = (int *) into[i];
                    *negated = -valueInt;
                }
            }
            else
            {
                if (blockTypes[i] == SCSREAL_N)
                {
                    // mapped as double
                    double* asDouble = (double*) into[i];
                    *asDouble = valueInt;
                }
                else
                {
                    int* asInteger = (int *) into[i];
                    *asInteger = valueInt;
                }
            }
        }
        else if (declaredTypes[i] == 3) // boolean
        {
            fmiBoolean v1Bool = 0;
            fmi2Boolean v2Bool = 0;
            int wideBool = 0;

            if (structFmu->version == workFMU::fmiV1 && structFmu->type == workFMU::ModelExchange)
            {
                status = structFmu->fmuME1->fmiGetBoolean((structFmu->instance), &sciOutputRefs[j], 1, &v1Bool);
                wideBool = v1Bool;
            }
            else if (structFmu->version == workFMU::fmiV1 && structFmu->type == workFMU::CoSimulation)
            {
                status = structFmu->fmuCS1->fmiGetBoolean((structFmu->instance), &sciOutputRefs[j], 1, &v1Bool);
                wideBool = v1Bool;
            }
            else if (structFmu->version == workFMU::fmiV2 && structFmu->type == workFMU::ModelExchange)
            {
                status2 = structFmu->fmuME2->fmi2GetBoolean((structFmu->instance), &sciOutputRefs[j], 1, &v2Bool);
                wideBool = v2Bool;
            }
            else if (structFmu->version == workFMU::fmiV2 && structFmu->type == workFMU::CoSimulation)
            {
                status2 = structFmu->fmuCS2->fmi2GetBoolean((structFmu->instance), &sciOutputRefs[j], 1, &v2Bool);
                wideBool = v2Bool;
            }

            FMU_SIM_PRINT("fmiGet_values integer valueReference %d set to %d\n", sciOutputRefs[j], wideBool);

            // store the highest possible error
            if (status > globalStatus)
            {
                globalStatus = status;
            }
            if (status2 > globalStatus2)
            {
                globalStatus2 = status2;
            }

            if ((sciOutputRefs[k] & NEGATED_ALIAS) == NEGATED_ALIAS) // negatedAlias
            {
                if (blockTypes[i] == SCSREAL_N)
                {
                    // mapped as double
                    double* asDouble = (double*) into[i];
                    *asDouble = !wideBool;
                }
                else
                {
                    char* asChar = (char *) into[i];
                    *asChar = !wideBool;
                }
            }
            else
            {
                if (blockTypes[i] == SCSREAL_N)
                {
                    // mapped as double
                    double* asDouble = (double*) into[i];
                    *asDouble = wideBool;
                }
                else
                {
                    char* asChar = (char *) into[i];
                    *asChar = wideBool;
                }
            }
        }
        else if (declaredTypes[i] == 4) // string
        {
            fmiString str;

            int maxStringLen = GetInPortSize(block, i, 2);
            if (structFmu->version == workFMU::fmiV1 && structFmu->type == workFMU::ModelExchange)
            {
                status = structFmu->fmuME1->fmiGetString((structFmu->instance), &sciOutputRefs[j], 1, &str);
            }
            else if (structFmu->version == workFMU::fmiV1 && structFmu->type == workFMU::CoSimulation)
            {
                status = structFmu->fmuCS1->fmiGetString((structFmu->instance), &sciOutputRefs[j], 1, &str);
            }
            else if (structFmu->version == workFMU::fmiV2 && structFmu->type == workFMU::ModelExchange)
            {
                status2 = structFmu->fmuME2->fmi2GetString((structFmu->instance), &sciOutputRefs[j], 1, &str);
            }
            else if (structFmu->version == workFMU::fmiV2 && structFmu->type == workFMU::CoSimulation)
            {
                status2 = structFmu->fmuCS2->fmi2GetString((structFmu->instance), &sciOutputRefs[j], 1, &str);
            }

            FMU_SIM_PRINT("fmiGet_values string valueReference %d set to %s\n", sciOutputRefs[j], str);

            // store the highest possible error
            if (status > globalStatus)
            {
                globalStatus = status;
            }
            if (status2 > globalStatus2)
            {
                globalStatus2 = status2;
            }

            char* asCharPtr = (char *) into[i];
            std::strncpy(asCharPtr, str, maxStringLen - 1);
            asCharPtr[maxStringLen] = '\0';
        }
    }

    // report the highest possible error
    if (globalStatus != fmiOK)
    {
        return globalStatus;
    }
    return globalStatus2;
}
void free_instance(scicos_block *block, int errorNumber, const char *functionName)
{
    workFMU *structFmu = *(workFMU **)(block->work);
    if (errorNumber)
    {
        Coserror("Error in simulation, returned by %s\n", functionName);
        set_block_error(errorNumber);
        
        std::wstring err = L"Error in FMU, " + scilab::UTF8::toWide(functionName) + L" function returned " + std::to_wstring(errorNumber) + L"\n";
        setLastErrorMessage(err.c_str());
    }

    if (structFmu->version == workFMU::fmiV1 && structFmu->type == workFMU::ModelExchange)
    {
        delete structFmu->fmuME1;
    }
    else if (structFmu->version == workFMU::fmiV1 && structFmu->type == workFMU::CoSimulation)
    {
        delete structFmu->fmuCS1;
    }
    else if (structFmu->version == workFMU::fmiV2 && structFmu->type == workFMU::ModelExchange)
    {
        delete structFmu->fmuME2;
    }
    else if (structFmu->version == workFMU::fmiV2 && structFmu->type == workFMU::CoSimulation)
    {
        delete structFmu->fmuCS2;
    }

    scicos_free(structFmu);
    *(block->work) = NULL;
}
