<?xml version="1.0" encoding="UTF-8"?>
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:ns5="http://www.w3.org/1999/xhtml" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:id="fmiModelVariables">
    <refnamediv>
        <refname>fmiModelVariables</refname>
        <refpurpose>Full information about model variables.</refpurpose>
    </refnamediv>
    <refsynopsisdiv>
        <title>Calling Sequence</title>
        <synopsis>
            vars = fmiModelVariables(fmu)
        </synopsis>
    </refsynopsisdiv>
    <refsection>
        <title>Arguments</title>
        <variablelist>
            <varlistentry>  
                <term>fmu</term>	
                <listitem>
                    <para>tlist, a model that can be received by <link linkend="importFMU">importFMU</link>.</para>
                </listitem>
            </varlistentry>
            <varlistentry>  
                <term>vars</term>	
                <listitem>
                    <para>tlist, full information about the model variables and type definitions.</para>
                    <simplelist type="inline">
                        <member>
                            <literal>vars.Real</literal> : list, all information for real variables of the model.
                        </member>
                        <member>
                            <literal>vars.Integer</literal> : list, all information for integer variables of the model.
                        </member>
                        <member>
                            <literal>vars.Boolean</literal> : list, all information for boolean variables of the model.
                        </member>
                        <member>
                            <literal>vars.String</literal> : list, all information for string variables of the model.
                        </member>
                        <member>
                            <literal>vars.Enumeration</literal> : list, all information for enumeration variables of the model.
                        </member>
                        <member>
                            <literal>vars.Type</literal> : list, all information for type definitions of the model.
                        </member>
                    </simplelist>
                </listitem>
            </varlistentry>
        </variablelist>
    </refsection>
    <refsection>
        <title>Description</title>
        <para>
            <literal>fmiModelVariables</literal> is the function that returns full information for all model variables. 
            It can be used for analysing of model descriptions in the case when there is not enough information about model variables.
            If any element of model variables definitions is not defined in XML file than appropriate field of list is empty. 
        </para>
    </refsection>
     
    <refsection>
        <title>Examples</title>
        <programlisting role="example">
            //Decompressing the model "SCADE_CruiseControl__CruiseControl_FMU". Creation a variable "fmu" of type tlist Scilab. 
			fmu = importFMU(pwd()+fullfile("\tests\unit_tests\SCADE_CruiseControl__CruiseControl_FMU.fmu"))
            //tlist, with full information about the model variables
            vars = fmiModelVariables(fmu)
            //names of variables
            vars.Real.name
            //references of variables
            vars.Real.valueReference
        </programlisting>
    </refsection>
     
    <refsection role="see also">
        <title>See Also</title>
        <simplelist type="inline">
            <member>
                <link linkend="fmiModelDescription">fmiModelDescription</link>
            </member>
        </simplelist>
    </refsection>
     
</refentry>
