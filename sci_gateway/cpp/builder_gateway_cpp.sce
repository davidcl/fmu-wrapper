function builder_gw_cpp()
    path = get_absolute_file_path("builder_gateway_cpp.sce")
    cflags = ilib_include_flag(fullpath(fullfile(path, "..", "..", "src", "c")));
    cflags = cflags + " " + ilib_include_flag(fullpath(fullfile(path, "..", "..", "src", "cpp")));
  
    tbx_build_gateway("fmu_wrapper_gw", ..
        ["fmu_link","fmu_link"; "fmu_ulink", "fmu_ulink"], ..
        ["fmu_link.cpp";"fmu_ulink.cpp"], ..
        path, ..
        ["../../src/cpp/libfmucpp"], ..
        "", ..
        cflags);

endfunction

builder_gw_cpp();
clear builder_gw_cpp; // remove builder_gw_cpp on stack
